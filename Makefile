CC = gcc
C_FLAGS = -c
PGM = keeplog
REF = listlib

all : $(PGM)

$(PGM) : $(REF).o $(PGM)_helper.o
	$(CC) $(C_FLAGS) $(REF).c
	$(CC) $(C_FLAGS) $(PGM)_helper.c
	$(CC) -o $(REF).o $(PGM)_helper.o

.c.o: $(PGM)_helper.h $(REF).h
	$(CC) $(C_FLAGS) $< -o $@

#$(REF).o : $(REF).c $(REF).h
#	$(CC) $(C_FLAGS) $(REF).c

#$(PGM).o : $(PGM).c $(PGM)_helper.h
#	$(CC) $(C_FLAGS) $(PGM).c

clean :
	rm $(PGM) *.o
